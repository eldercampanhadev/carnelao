package br.com.app.carnelao.presentation.ui.pause;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import br.com.app.carnelao.R;
import br.com.app.carnelao.presentation.ui.helper.TextUtils;
import br.com.app.carnelao.presentation.ui.info.InfoActivity;
import br.com.app.carnelao.presentation.ui.login.LoginActivity;
import br.com.app.carnelao.presentation.ui.playscreen.PlayActivity;
import br.com.app.carnelao.util.Constants;

public class PauseActivity extends AppCompatActivity {

    private TextView lblPause;
    private Button btnBack;
    private Button btnContinue;
    private Button btnExit;
    private Button btnHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause);

        // AdMod
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        lblPause = (TextView)findViewById(R.id.lbl_pause);
        btnBack = (Button) findViewById(R.id.btn_restart);
        btnContinue = (Button)findViewById(R.id.btn_play_again);
        btnExit = (Button)findViewById(R.id.btn_exit);
        btnHelp = (Button)findViewById(R.id.btn_help);

        // fonts
        setFonts();

    }

    private void setFonts() {
        TextUtils.setFont(lblPause, Constants.Fonts.TITLE_FONT, this);
        TextUtils.setFont(btnBack, Constants.Fonts.BUTTON_FONT, this);
        TextUtils.setFont(btnContinue, Constants.Fonts.BUTTON_FONT, this);
        TextUtils.setFont(btnExit, Constants.Fonts.BUTTON_FONT, this);
        TextUtils.setFont(btnHelp, Constants.Fonts.BUTTON_FONT, this);
    }

    public void btnRestartClicked(View button){
        Intent intent = new Intent(PauseActivity.this,
                PlayActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void btnExitClicked(View button){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void btnContinueClicked(View button){
        finish();
    }

    public void btnHelpClicked(View view){
        Intent intent = new Intent(PauseActivity.this, InfoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
