package br.com.app.carnelao.presentation.ui.info;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.TextView;

import br.com.app.carnelao.R;
import br.com.app.carnelao.presentation.ui.helper.TextUtils;
import br.com.app.carnelao.util.Constants;

public class InfoActivity extends AppCompatActivity {

    private TextView lblInfoRight;
    private TextView lblInfoLeft;
    private TextView lblInfoTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        lblInfoLeft = (TextView)findViewById(R.id.lbl_info_left);
        lblInfoRight = (TextView)findViewById(R.id.lbl_info_right);
        lblInfoTitle = (TextView)findViewById(R.id.lbl_info_title);


        // set font
        TextUtils.setFont(lblInfoTitle, Constants.Fonts.BUTTON_FONT, this);
        TextUtils.setFont(lblInfoLeft, Constants.Fonts.SUBTITLE_FONT, this);
        TextUtils.setFont(lblInfoRight, Constants.Fonts.SUBTITLE_FONT, this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }
}
